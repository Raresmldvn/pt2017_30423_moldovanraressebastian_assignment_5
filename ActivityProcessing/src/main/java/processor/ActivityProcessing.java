package processor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Class that implements the processing of the data received in the log file.
 * @author rares
 * @version 1.0
 * @since 05-14-2017
 */
public class ActivityProcessing {
	/**
	 * Method that computes the number of separate days found in the log.
	 * @param list Parameter that represents the list obtained from the text file filled with objects of type MonitoredData.
	 * @return Returns number of separate days in the form of a long variable.
	 */
	private static long numberOfDays(ArrayList<MonitoredData> list) {
		
		List<String> startDays = list.stream().map(a -> a.getStartTime().substring(0, 10)).distinct().collect(Collectors.toList()); 
		List<String> endDays = list.stream().map(a -> a.getEndTime().substring(0, 10)).distinct().collect(Collectors.toList());
		startDays.addAll(endDays);
		return  startDays.stream().distinct().count();
	}
	
	/**
	 * Method that maps in a collection of type map every activity with the correspondent number of occurrences.
	 * @param list Parameter that represents the list obtained from the text file filled with objects of type MonitoredData.
	 * @return Returns the built map.
	 */
	private static Map<String, Long> actionToOccurence(ArrayList<MonitoredData> list) {
		
		Map<String, Long> actionToOccurence = list.stream().collect(Collectors.groupingBy(a -> a.getType().toString(), 
				Collectors.counting()));
		return actionToOccurence;
	}
	
	/**
	 * Method that maps in a collection of type map every day to its corresponding map of activities and occurrences.
	 * @param list Parameter that represents the list obtained from the text file filled with objects of type MonitoredData.
	 * @return Returns the built map.
	 */
	private static Map<Integer, Map<String, Long>> dayActionToOccurence(ArrayList<MonitoredData> list) {
		
		Map<String, Map<String, Long>> finalList = list.stream().collect(Collectors.groupingBy(MonitoredData::getDay, 
				Collectors.groupingBy(MonitoredData::getTypeName, Collectors.counting())));
		
		Map<Integer, Map<String, Long>> actualList = new HashMap<Integer, Map<String, Long>>();
		int[] i = {0};
		finalList.forEach((k, v) -> {i[0]++;
									actualList.put(new Integer(i[0]), v); });
		return actualList;
	}
	
	/**
	 * Method that associates with each method the total time taken in the log and filters by verifying that the total time overcomes 10 hours.
	 * @param list Parameter that represents the list obtained from the text file filled with objects of type MonitoredData.
	 * @return Returns the built map.
	 */
	private static Map<String, Long> activitiesTime(ArrayList<MonitoredData> list) {
		
		Map<String, Long> firstList = list.stream().collect(Collectors.groupingBy(a-> a.getType().toString(), Collectors.summingLong(a->a.subtractTimes())));
		firstList.keySet().forEach((String key) -> { 	
														Long value = firstList.get(key);
														firstList.put(key, new Long(TimeUnit.MILLISECONDS.toSeconds(value))); });
		Map<String, Long> finalList = firstList.entrySet().stream().
				filter(map -> map.getValue()>10*60*60).collect(Collectors.toMap(e -> e.getKey(), e->e.getValue()));
		return finalList;
	}
	
	/**
	 * Method that filters only the activities that have 90% of their occurrences with a time of less than 5 minutes.
	 * @param list Parameter that represents the list obtained from the text file filled with objects of type MonitoredData.
	 * @return Returns the built list of strings.
	 */
	private static List<String> filteredActions(ArrayList<MonitoredData> list) {
		
		Map<String, Long> actionToOccurence = actionToOccurence(list);
		Map<String, Long> acctionToOccurenceLessMinutes = list.stream().filter(a -> TimeUnit.MILLISECONDS.toMinutes(a.subtractTimes())<=5).collect(Collectors.groupingBy(a -> a.getType().toString(), 
				Collectors.counting()));
		
		List<String> finalList = actionToOccurence.entrySet().stream().filter(a -> {
										if(a.getValue()!=null&&acctionToOccurenceLessMinutes.get(a.getKey())!=null)
										return 0.9*a.getValue()<= acctionToOccurenceLessMinutes.get(a.getKey());
										return false;}).map(a -> a.getKey()).collect(Collectors.toList());
		return finalList;
		
		
	}
	
	public static void main(String args[]) {
		
		IOFile fileProcessor = new IOFile();
		ArrayList<MonitoredData> initialList = fileProcessor.readActivities();
		System.out.println("Number of separate days: " + numberOfDays(initialList));
		fileProcessor.writeOccurences(actionToOccurence(initialList));
		fileProcessor.writeTypeOccurencesPerDay(dayActionToOccurence(initialList));
		fileProcessor.writeDurations(activitiesTime(initialList));
		fileProcessor.writeFiltered(filteredActions(initialList));
	}

}
