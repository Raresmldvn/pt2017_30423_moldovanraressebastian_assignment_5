package processor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Class that implements the necessary operations for reading/writing in text files.
 * @author rares
 * @version 1.0
 * @since 05-14-2017
 *
 */
public class IOFile {

	/**
	 * Method that opens the file Activities.txt, reads line by line and constructs an array list with all the objects of type MonitoredData.
	 * The type of the activity is decided using the first 3 characters in the type name field.
	 * @return Returns the built list.
	 */
	public ArrayList<MonitoredData> readActivities() {
		
		BufferedReader br = null;
		try{
		br = new BufferedReader(new FileReader("Activities.txt"));
		} catch(FileNotFoundException e)  {
			
			System.out.println(e.getMessage());
		}
		String line;
		ArrayList<MonitoredData> outputList = new ArrayList<MonitoredData>();
		try {
		while((line = br.readLine())!=null) {
			String startTime = line.substring(0, 19);
			String endTime = line.substring(21, 40);
			switch(line.substring(42, 45)){
			case "Sle": {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Sleeping));
				break;
			}
			case "Toi": {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Toileting));
				break;
			}
			case "Sho" : {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Showering));
				break;
			}
			case "Bre" : {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Breakfast));
				break;
			}
			case "Lea" : {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Leaving));
				break;
			}
			case "Lun" : {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Lunch));
				break;
			}
			case "Spa" :{
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Spare_Time));
				break;
			}
			case "Gro" :{
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Grooming));
				break;
			}
			case "Sna" : {
				outputList.add(new MonitoredData(startTime, endTime, MonitoredData.Type.Snack));
				break;
			}
			}
		}
		}catch(Exception e) {
			
			System.out.println(e.getMessage() + e.getCause());
		}
		
		return outputList;
	}
	
	/**
	 * Method that writes a map containing String keys and Long values into a file using buffered writing.
	 * @param occurenceMap Parameter that represents the map to be written.
	 */
	public void writeOccurences(Map<String, Long> occurenceMap) {
		
		try{
	        FileOutputStream fileOut = new FileOutputStream("Occurences.txt");
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOut));
	        for(String s: occurenceMap.keySet()) {
	        	bw.write(s + " "+ occurenceMap.get(s));
	        	bw.newLine();
	        }
	        bw.close();
		}catch(IOException e) {
			
		e.printStackTrace();
		}
	}
	
	/**
	 * Method that writes a map containing String keys and mapped values into a file using buffered writing.
	 * @param occurenceMap Parameter that represents the map to be written.
	 */
	public void writeTypeOccurencesPerDay(Map<Integer, Map<String, Long>> occurenceMap) {
		
		try{
			boolean first=true;
	        FileOutputStream fileOut = new FileOutputStream("DayOccurences.txt");
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOut));
	        for(Integer s: occurenceMap.keySet()) {
	        	bw.write("Day number " + s + "");
	        	first = true;
	        	for(String a: occurenceMap.get(s).keySet()){
	        		if(first==true){
	        		bw.write("           " + a + "  "+ occurenceMap.get(s).get(a));
	        		first = false;
	        		} else {
	        			bw.write("                       " + a + "  "+ occurenceMap.get(s).get(a));
	        		}
	        		bw.newLine();
	        	}
	        }
	        bw.close();
		}catch(IOException e) {
			
		e.printStackTrace();
		}
	}
	/**
	 * Method that writes a map containing String keys and Long values into a file using buffered writing.
	 * @param occurenceMap Parameter that represents the map to be written.
	 */
	public void writeDurations(Map<String, Long> durations) {
		
		try{
	        FileOutputStream fileOut = new FileOutputStream("Durations.txt");
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOut));
	        for(String s: durations.keySet()) {
	        	long hours = TimeUnit.SECONDS.toHours(durations.get(s));
	        	long minutes = TimeUnit.SECONDS.toMinutes(durations.get(s)-hours*3600);
	        	long seconds = durations.get(s) - hours*3600 - minutes*60;
	        	bw.write(s + " " + hours + ":" + minutes + ":" + seconds);
	        	bw.newLine();
	        }
	        bw.close();
		}catch(IOException e) {
			
		e.printStackTrace();
		}
	}
	
	/**
	 * Method that writes a list of strings into a text file.
	 * @param filtered Parameter that represents a list of strings.
	 */
	public void writeFiltered(List<String> filtered) {
		
		try{
	        FileOutputStream fileOut = new FileOutputStream("Filtered.txt");
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOut));
	        for(String s: filtered) {
	        	bw.write(s);
	        	bw.newLine();
	        }
	        bw.close();
		}catch(IOException e) {
			
		e.printStackTrace();
		}
	}
}
