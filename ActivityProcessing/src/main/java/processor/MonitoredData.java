package processor;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Class that models a single data received from the activity log.
 * @author rares
 * @version 1.1
 * @since 05-13-2017
 */
public class MonitoredData {

	private String startTime;
	private String endTime;
	private Type type;
	
	public enum Type {Leaving, Toileting, Showering, Sleeping, Breakfast, Lunch, Snack, Spare_Time, Grooming};
	/**
	 * Simple constructor for initializing the fields.
	 */
	public MonitoredData() {
		
		this.startTime = new String();
		this.endTime = new String();
		this.type = null;
	}
	/**
	 * Explicit constructor for initializing the attributes of the object.
	 * @param start Represents the start time.
	 * @param end Represents the end time.
	 * @param type Represents the type of activity.
	 */
	public MonitoredData(String start, String end, Type type) {
		
		this.startTime = start;
		this.endTime = end;
		this.type = type;
	}
	/**
	 * Getter for the start time.
	 * @return Returns the start time attribute of the object.
	 */
	public String getStartTime() {
		
		return startTime;
	}
	
	/**
	 * Getter for the end time.
	 * @return Returns the end time attribute of the object.
	 */
	public String getEndTime() {
		
		return endTime;
	}
	
	/**
	 * Getter for the type.
	 * @return Returns the type attribute of the object as value from the enumeration.
	 */
	public Type getType() {
		
		return type;
	}
	/**
	 * Getter for the type name.
	 * @return Returns the type attribute of the object as a string.
	 */
	public String getTypeName() {
		
		return type.toString();
	}
	/**
	 * Method that represents the object as a string.
	 */
	public String toString() {
		
		return startTime.toString() + "		" + endTime.toString() + "		" + type.toString();
	}
	
	/**
	 * Method that defines the equality between 2 objects. In this case, 2 objects are considered equal for equal start times.
	 */
	public boolean equals(Object obj) {
		return this.getStartTime().substring(0,10).equals(((MonitoredData) obj).getStartTime().substring(0,10));
	}
	
	/**
	 * Getter for the day in the start time representation.
	 * @return Returns the day as 10-character string.
	 */
	public String getDay() {
		
		return getStartTime().substring(0,10);
	}
	
	/**
	 * Method for subtracting the end time from the start time.
	 * @return Returns the value in millisecond of the time difference.
	 */
	public long subtractTimes(){
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = new Date();
		Date date2 = new Date();
		try{
		date1 = sdf.parse(startTime);
		date2 = sdf.parse(endTime);
		}catch(Exception e) {}
		return date2.getTime() - date1.getTime();
	}
	
}
